# Rock Paper Scissors #

Sample project using Node JS, Express Framework, Socket-IO, Firmata and Arduino.

You have...


* Arduino uno Board.
* 2 Servos 9gr.
* NodeJS and express framework Installed.
* LED's (Red, Blue and Green  Ideally).
* Resistors of 220 Ohms


How to run?


* Open Arduino IDE
* Verifiy the listen port
