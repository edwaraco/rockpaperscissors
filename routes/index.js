var rockpaperscissors = require('../app/rockpaperscissors');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Arduino connectivity Rock-paper-scissors game' });
});

/* GET Hello World page. */
router.get('/rockpaperscissors', function(req, res) {
    res.render('rockpaperscissors', { title: 'Arduino connectivity Rock-paper-scissors game' });
});

router.get('/rockpaperscissors/play', function(req, res) {
    var result = rockpaperscissors.play(parseInt(req.query.optionSel));
    res.json(result);
});

module.exports = router;
