console.log("Init settings")
var myVictories = 0
var arduinoVictories = 0
function sumVictory(dataJson) {
  if (dataJson.victory == "Arduino win.") {
    arduinoVictories++;
  } else if (dataJson.victory == "I win.") {
    myVictories++;
  }
}
var game = function(optionSel) {
  $.ajax({
    url: "rockpaperscissors/play",
    method: "GET",
    data: { "optionSel" : optionSel },
    dataType: "json",
    success: function(dataJson){
      $("#inData").append("You select a " + dataJson.myOptionSelDescription + "\r");
      $("#inData").append("Arduino select a " + dataJson.arduinoOptionDescription + "\r");
      $("#inData").append(dataJson.victory + "\r");
      $("#inData").animate({scrollTop:$("#inData")[0].scrollHeight - $("#inData").height()},200);
      sumVictory(dataJson);
      $("#val_win").text(myVictories);
      $("#val_fail").text(arduinoVictories);
    }
  });
};

$('#rockSel').click(function() {
  game(0);
});
$('#paperSel').click(function() {
  game(1);
});
$('#scissorsSel').click(function() {
  game(2);
});
