var Arduino = require('../app/arduino.js');
var io = require('socket.io-client');
var coreGame = {
}
coreGame.play = function(optionSel) {
  var result = {};
  var socket = io.connect('http://localhost:3001');
  var arduinoOption = this.getArduinoOption();
  result.arduinoOptionCode = arduinoOption;
  result.arduinoOptionDescription = this.getDescriptionSel(arduinoOption);
  result.myOptionSelCode = optionSel;
  result.myOptionSelDescription = this.getDescriptionSel(optionSel);
  result.victory = this.getWinner(optionSel, arduinoOption);
  socket.emit('servo',{"arduinoSel": arduinoOption});
  socket.emit('led',{"OptionSel": optionSel});
  return result;
}

coreGame.getArduinoOption = function () {
  return Math.floor(Math.random() * (3));
}

coreGame.getDescriptionSel = function (optionSel) {
  var description = "";
  switch (optionSel) {
    case 0:
      description = "Rock";
      break;
    case 1:
      description = "Paper";
      break;
    case 2:
      description = "Scissors";
      break;
    default:
      description = "";
    break;
  }
  return description;
}

coreGame.isWinnerPaper = function(option1, option2) {
  return option1 == 0 && option2 == 1;
}

coreGame.isWinnerRock = function(option1, option2) {
  return option1 == 2 && option2 == 0;
}

coreGame.isWinnerScissors = function(option1, option2) {
  return option1 == 1 && option2 == 2;
}

coreGame.getWinner = function(miOptionSel, optionSelArduino) {
  if (miOptionSel == optionSelArduino) {
    return "Empate!!!.";
  } else if (this.isWinnerRock(miOptionSel, optionSelArduino) ||
      this.isWinnerPaper(miOptionSel, optionSelArduino) ||
      this.isWinnerScissors(miOptionSel, optionSelArduino)) {
    return "Arduino win.";
  } else {
    return "I win.";
  }
}

module.exports = coreGame;
