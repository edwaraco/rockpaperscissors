var Board = require("firmata").Board;
var board = new Board("/dev/ttyACM0");
var express = require('express');
var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);
server.listen(3001);

var Arduino = {
}
Arduino.initConfig = function () {
  board.on("ready", function() {
      var createServoMotor = function(pin, angle) {
        // This will map 0-180 to 1000-1500
        board.pinMode(pin, board.MODES.SERVO);
        board.servoConfig(pin, 1000, 1500);
        moveServo(pin, angle);
      };
      var createLed = function (pin) {
         board.pinMode(pin, board.MODES.OUTPUT);
         board.digitalWrite(pin, 0);
      }
      createServoMotor(7, 0);
      createServoMotor(8, 180);
      createServoMotor(9, 0);
      createLed(4);
      createLed(5);
      createLed(6);
    });
};
Arduino.initConfig();
var moveServo = function (pin, angle) {
  setTimeout(function(){board.servoWrite(pin, angle);}, 100);
}
// on a socket connection
io.sockets.on('connection', function (socket) {
  // if servo message received
  socket.on('led', function (data) {
    function viewLed(leds) {
      for (led in leds) {
        board.digitalWrite(led, leds[led]);
      }
    };
    if(board.isReady) {
      switch (data.OptionSel) {
        case 0:
            viewLed({4: 1, 5:0, 6:0});
          break;
        case 1:
            viewLed({4: 0, 5:1, 6:0});
          break;
        case 2:
            viewLed({4: 0, 5:0, 6:1});
          break;
        default:
          viewLed({4: 0, 5:0, 6:0});
          break;
      };
    }
  });
  socket.on('servo', function (data) {
    if(board.isReady) {
      switch (data.arduinoSel) {
        case 0:
          moveServo(7, 0);
          moveServo(8, 180);
          moveServo(9, 180);
          break;
        case 1:
          moveServo(7, 180);
          moveServo(8, 0);
          moveServo(9, 0);
          break;
        case 2:
          moveServo(7, 180);
          moveServo(8, 180);
          moveServo(9, 180);
        break;
      }
    }
  });
});

module.exports = Arduino;
